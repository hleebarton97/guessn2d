using System;
using UnityEditor;
using UnityEngine.SceneManagement;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Build {

        ///////////////////////////////////////////////////////////////////
        // G L O B A L   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        private static string[] REQUIRED_ENV_VARS = new string[] {
            "ANDROID_APK_FILENAME",
            "ANDROID_APK_OUTPUT_DIR",
            "ANDROID_KEYSTORE",
            "ANDROID_KEYSTORE_ALIAS",
            "ANDROID_KEYSTORE_ALIAS_PASSWORD",
            "ANDROID_KEYSTORE_PASSWORD",
            "ANDROID_SDK_ROOT",
            "ANDROID_NDK_ROOT",
            "UNITY_PATH"
        };

        ///////////////////////////////////////////////////////////////////
        // B U I L D   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        // Configure android build and create APK file
        public static void StartAndroidBuild () {
            // Check for missing required environment variables
            if (IsMissingEnvironmentVariables(REQUIRED_ENV_VARS)) {
                Environment.ExitCode = -1;
                return;
            }

            // Configure android build player settings
            Console.WriteLine($"*** Player Settings Bundle Version: {PlayerSettings.bundleVersion}");
            PlayerSettings.Android.bundleVersionCode = GetBundleVersionCode();
            Console.WriteLine($"*** Android Bundle Version Code: {PlayerSettings.Android.bundleVersionCode}");
            PlayerSettings.Android.keystoreName = Environment.GetEnvironmentVariable("ANDROID_KEYSTORE");
            PlayerSettings.Android.keystorePass = Environment.GetEnvironmentVariable("ANDROID_KEYSTORE_PASSWORD");
            PlayerSettings.Android.keyaliasName = Environment.GetEnvironmentVariable("ANDROID_KEYSTORE_ALIAS");
            PlayerSettings.Android.keyaliasPass = Environment.GetEnvironmentVariable("ANDROID_KEYSTORE_ALIAS_PASSWORD");

            string[] commandLineArgs = Environment.GetCommandLineArgs();
            string apkFilePath = commandLineArgs[commandLineArgs.Length - 1];

            // Start build process
            BuildPipeline.BuildPlayer(GetScenePathsList(), apkFilePath, BuildTarget.Android, BuildOptions.None);
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        // Check if any environment required are undefined
        private static bool IsMissingEnvironmentVariables (string[] variables) {
            string value;
            foreach (string variable in variables) {
                value = Environment.GetEnvironmentVariable(variable);

                if (Util.IsNull(value)) {
                    Console.WriteLine($"*** ERROR :: Build :: Missing environment variable -> {variable}");
                    Console.WriteLine($"*** Stopping build process!");
                    return true;
                }
            }

            return false;
        }


        // Get the path for each scene in build settings list
        private static string[] GetScenePathsList () {
            string[] scenePaths = new string[SceneManager.sceneCountInBuildSettings];

            Console.WriteLine($"*** Scene Count In Build Settings: {SceneManager.sceneCountInBuildSettings}");

            for (int i = 0; i < scenePaths.Length; i++) {
                scenePaths[i] = SceneUtility.GetScenePathByBuildIndex(i);
                Console.WriteLine($"*** Scene Path At Build Index {i}: {scenePaths[i]} <- {SceneUtility.GetScenePathByBuildIndex(i)}");
            }

            return scenePaths;
        }

        // Create bundle version code from bundle version in PlayerSettings
        private static int GetBundleVersionCode () {
            string[] versionValues = PlayerSettings.bundleVersion.Split('.');
            return int.Parse(String.Join("", versionValues));
        }
    }
}
