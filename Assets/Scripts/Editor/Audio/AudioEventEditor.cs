using UnityEngine;
using UnityEditor;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    [CustomEditor(typeof(AudioEventSO), true)]
    public class AudioEventEditor : Editor {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private AudioSource audioPreview;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void OnEnable () {
            this.audioPreview = EditorUtility.CreateGameObjectWithHideFlags(
                "Audio Event Preview",
                HideFlags.HideAndDontSave,
                typeof(AudioSource)
            ).GetComponent<AudioSource>();
        }

        public void OnDisable () {
            DestroyImmediate(this.audioPreview.gameObject);
        }

        public override void OnInspectorGUI () {
            this.DrawDefaultInspector();

            EditorGUI.BeginDisabledGroup(this.serializedObject.isEditingMultipleObjects);

            if (GUILayout.Button("Play Audio Clip")) {
                ((AudioEventSO) this.target).Play(this.audioPreview);
            }

            EditorGUI.EndDisabledGroup();
        }
    }
}
