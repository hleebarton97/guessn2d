#if UNITY_IOS
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;



// Novasloth Games LLC
// Lee Barton
public class TrackingDescriptionPostBuild {

    ///////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    ///////////////////////////////////////////////////////////////////

    private const string TRACKING_DESCRIPTION = "We try to show ads for apps and products that will be most interesting to you based on the apps you use, the device you are on, and the country you are in.";

    ///////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    ///////////////////////////////////////////////////////////////////

    public static void OnPostProcessBuild (BuildTarget buildTarget, string pathToXcode) {
        if (buildTarget == BuildTarget.iOS) {
            AddPListValues(pathToXcode);
        }
    }

    ///////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    ///////////////////////////////////////////////////////////////////

    private static void AddPListValues (string pathToXcode) {
        // Get plist file from Xcode directory
        string plistPath = $"{pathToXcode}/Info.plist";
        PlistDocument plistDocument = new PlistDocument();

        // Read plist file values
        plistDocument.ReadFromString(File.ReadAllText(plistPath));

        // Set values from the root
        PlistElementDict plistRoot = plistDocument.root;

        // Set description in plist
        plistRoot.SetString("NSUserTrackingUsageDescription", TRACKING_DESCRIPTION);

        // Save changes to plist
        File.WriteAllText(plistPath, plistDocument.WriteToString());
    }
}
#endif