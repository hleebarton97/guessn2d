using UnityEngine;
using UnityEngine.SceneManagement;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    public enum Scene {
        START_MENU,
        GAME,
        GAME_OVER,
        SCORES
    }

    public class SceneLoader : MonoBehaviour, IAwakable, IEventRegisterable {

        private static GameObject Instance;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
            this.SetupReferences();
            this.Init();
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () { }

        public void Init () {
            if (!Util.IsNull(Instance))
                Destroy(Instance);

            Instance = this.gameObject;
            DontDestroyOnLoad(this);
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void LoadStartMenuScene () {
           SceneManager.LoadScene((int)Scene.START_MENU);
        }

        private void LoadGameScene () {
            SceneManager.LoadScene((int)Scene.GAME);
        }

        private void LoadGameOverScene () {
            SceneManager.LoadScene((int)Scene.GAME_OVER);
        }

        private void LoadScoresScene () {
            SceneManager.LoadScene((int)Scene.SCORES);
        }

        private void ExitGame () {
            Application.Quit();
        }

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {
            StartMenuEvents.PlayEvent += this.OnPlay;
            StartMenuEvents.ShowScoresEvent += this.OnScores;
            StartMenuEvents.ExitEvent += this.OnExit;

            GameEvents.GameOverEvent += this.OnGameOver;

            GameOverEvents.PlayAgainEvent += this.OnPlayAgain;
            GameOverEvents.QuitEvent += this.OnQuit;

            ScoresEvents.BackEvent += this.OnBack;
        }

        public void DeregisterEventHandlers () {
            StartMenuEvents.PlayEvent -= this.OnPlay;
            StartMenuEvents.ShowScoresEvent -= this.OnScores;
            StartMenuEvents.ExitEvent -= this.OnExit;

            GameEvents.GameOverEvent -= this.OnGameOver;

            GameOverEvents.PlayAgainEvent -= this.OnPlayAgain;
            GameOverEvents.QuitEvent -= this.OnQuit;

            ScoresEvents.BackEvent -= this.OnBack;
        }

        // Start Menu
        private void OnPlay () {
            this.LoadGameScene();
        }

        private void OnScores () {
            this.LoadScoresScene();
        }

        private void OnExit () {
            this.ExitGame();
        }

        // Game
        private void OnGameOver () {
            this.LoadGameOverScene();
        }


        // Game Over
        private void OnPlayAgain () {
            this.OnPlay();
        }

        private void OnQuit () {
            this.LoadStartMenuScene();
        }

        // Scores
        private void OnBack () {
            this.LoadStartMenuScene();
        }
    }
}
