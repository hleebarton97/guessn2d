using UnityEngine;
#if UNITY_IOS
using Unity.Advertisement.IosSupport;
#endif

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class AttPermissionRequest : MonoBehaviour {
        private void Awake () {
#if UNITY_IOS
            if (ATTrackingStatusBinding.GetAuthorizationTrackingStatus() == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED) {
                ATTrackingStatusBinding.RequestAuthorizationTracking();
            }
#endif
        }
    }
}
