using UnityEngine;
using UnityEngine.Advertisements;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class AdsButton : Button, IUnityAdsLoadListener, IUnityAdsShowListener {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [Header("Temp Buttons")]
        [SerializeField] private GameObject loadingButton;
        [SerializeField] private GameObject errorButton;

        private const string androidAdUnitId = "Rewarded_Android";
        private const string iOSAdUnitId = "Rewarded_iOS";
        private string adUnitId = null;

        private bool willShowAd = false;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        protected override void Awake () {
            base.Awake();

            this.willShowAd = (Random.Range(0.0f, 1.0f) <= 0.45);

            Novalog.Info("AdsButton", $"Ad will play for second chance: {this.willShowAd}");
            this.gameObject.SetActive(!this.willShowAd);
            this.loadingButton.SetActive(this.willShowAd);
            this.errorButton.SetActive(false);
            if (this.willShowAd) {
                this.InitAd();
            }
        }

        public void OnMouseUp () {
            if (this.willShowAd) {
                Novalog.Info("AdsButton", "Playing ad for second chance...");
                this.ShowAd();
            } else {
                Novalog.Info("AdsButton", "Skipping ad and giving second chance...");
                GameEvents.AcceptSecondChance();
            }
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void InitAd () {
#if UNITY_IOS
            this.adUnitId = iOSAdUnitId;
#elif UNITY_ANDROID
            this.adUnitId = androidAdUnitId;
#endif

            this.LoadAd();
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void LoadAd () {
            Novalog.Info("AdsButton", $"Loading content into Ad Unit -> {this.adUnitId}");
            Advertisement.Load(this.adUnitId, this);
        }

        private void ShowAd () {
            Novalog.Info("AdsButton", $"Showing ad -> {this.adUnitId}");
            Advertisement.Show(this.adUnitId, this);
        }

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   A D   L I S T E N E R S
        ///////////////////////////////////////////////////////////////////

        public void OnUnityAdsAdLoaded (string placementId) {
            Novalog.Info("AdsButton", $"Ad loaded -> {this.adUnitId}");

            if (this.adUnitId.Equals(placementId)) {
                this.loadingButton.SetActive(false);
                this.gameObject.SetActive(true);
            }
        }

        public void OnUnityAdsFailedToLoad (string placementId, UnityAdsLoadError error, string message) {
            Novalog.Error("AdsButton", $"Ad failed to load Ad Unit -> {placementId}! -> {error.ToString()} :: {message}");
            this.errorButton.SetActive(true);
            this.loadingButton.SetActive(false);
            this.gameObject.SetActive(false);
        }

        public void OnUnityAdsShowFailure (string placementId, UnityAdsShowError error, string message) {
            Novalog.Error("AdsButton", $"Ad failed to show Ad Unit -> {placementId}! -> {error.ToString()} :: {message}");
            GameEvents.GameOver();
        }

        public void OnUnityAdsShowStart (string placementId) {}

        public void OnUnityAdsShowClick (string placementId) {}

        // @INFO Must be tested via build
        // Unity test ads to not trigger Complete callback
        public void OnUnityAdsShowComplete (string placementId, UnityAdsShowCompletionState showCompletionState) {
            Novalog.Info("AdsButton", $"Ad finished showing -> {placementId} :: Completion State -> {showCompletionState}");

            bool completeState = showCompletionState.Equals(UnityAdsShowCompletionState.COMPLETED);

            if (this.adUnitId.Equals(placementId) && completeState) {
                Novalog.Info("AdsButton", $"Rewarded Ad completed!");
                GameEvents.AcceptSecondChance();
            } else {
                GameEvents.GameOver();
            }
        }
    }
}
