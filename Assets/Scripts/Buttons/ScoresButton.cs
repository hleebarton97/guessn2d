
// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class ScoresButton : Button {

        private void OnMouseUp () {
            StartMenuEvents.ShowScores();
        }
    }
}
