using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class PlayAgainButton : Button {

        //protected override void PressButton () {
        //    base.PressButton();

        //    GameOverEvents.PlayAgain();
        //}

        private void OnMouseUp () {
            GameOverEvents.PlayAgain();
        }
    }
}
