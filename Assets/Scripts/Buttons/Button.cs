using UnityEngine;
using UnityEngine.Audio;
using TMPro;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Button : MonoBehaviour, IAwakable {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public bool IsPressed { get; set; } = false;

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [Header("SO")]
        [SerializeField] protected ButtonSO _Button;

        [Header("Sprites")]
        [SerializeField] protected TextMeshProUGUI buttonDefaultUI;
        [SerializeField] protected TextMeshProUGUI buttonPressedUI;

        [Header("SFX")]
        [SerializeField] protected AudioEventSO _AudioEvent;
        [SerializeField] protected AudioSource audioSource;

        private GameObject spriteDefault;
        private GameObject spritePressed;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        protected virtual void Awake () {
            this.SetupReferences();
            this.Init();
        }

        public void OnMouseDown () {
            if (!this.IsPressed) {
                this.PressButton();
            }
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public virtual void SetupReferences () {
            this.SetupAudioSource();
        }

        public virtual void Init () {
            this.spriteDefault = this.transform.GetChild(0).gameObject;
            this.spritePressed = this.transform.GetChild(1).gameObject;

            if (!Util.IsNull(this._Button)) {
                this.buttonDefaultUI.text = this._Button.Text;
                this.buttonPressedUI.text = this._Button.Text;
            }
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        protected virtual void PressButton () {
            Novalog.Info("Button", $"Button pressed!");
            this._AudioEvent.Play(this.audioSource);
            this.IsPressed = true;
            this.spriteDefault.SetActive(false);
            this.spritePressed.SetActive(true);
        }

        protected virtual void FixButton () {
            this.IsPressed = false;
            this.spriteDefault.SetActive(true);
            this.spritePressed.SetActive(false);
        }

        private void SetupAudioSource () {
            if (Util.IsNull(this.audioSource)) {
                this.audioSource = Util.SetupAudioSource(this.gameObject);
            }
        }
    }
}
