using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class SocialButton : Button {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [Header("SO")]
        [SerializeField] private SocialButtonSO _SocialButton;

        private void OnMouseUp () {
            Novalog.Info("SocialButton", $"Redirecting to -> {_SocialButton.Link}");
            Application.OpenURL(_SocialButton.Link);
            this.FixButton();
        }
    }
}
