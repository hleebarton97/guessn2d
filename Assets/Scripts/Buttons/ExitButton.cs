using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class ExitButton : Button {

        private void OnMouseUp () {
            StartMenuEvents.Exit();
        }
    }
}
