using UnityEngine;
using UnityEngine.Advertisements;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class QuitButton : Button, IUnityAdsLoadListener, IUnityAdsShowListener {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        private const string androidAdUnitId = "Interstitial_Android";
        private const string iOSAdUnitId = "Interstitial_iOS";
        private string adUnitId = null;
        private bool failedToLoadAd = false;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        protected override void Awake () {
            base.Awake();

            //this.InitAd();
        }

        private void OnMouseUp () {
            GameOverEvents.Quit();
            //if (!this.failedToLoadAd) {
            //    this.ShowAd();
            //} else {
            //    GameOverEvents.Quit();
            //}
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void InitAd () {
            this.adUnitId = (Application.platform == RuntimePlatform.IPhonePlayer)
                ? iOSAdUnitId
                : androidAdUnitId;

            this.LoadAd();
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void LoadAd () {
            Novalog.Info("QuitButton", $"Loading content into Ad Unit -> {this.adUnitId}");
            Advertisement.Load(this.adUnitId, this);
        }

        private void ShowAd () {
            Novalog.Info("QuitButton", $"Showing ad -> {this.adUnitId}");
            Advertisement.Show(this.adUnitId, this);
        }

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   A D   L I S T E N E R S
        ///////////////////////////////////////////////////////////////////

        public void OnUnityAdsAdLoaded (string placementId) {
            Novalog.Info("QuitButton", $"Ad loaded -> {this.adUnitId}");
        }

        public void OnUnityAdsFailedToLoad (string placementId, UnityAdsLoadError error, string message) {
            Novalog.Error("QuitButton", $"Ad failed to load Ad Unit -> {placementId}! -> {error.ToString()} :: {message}");
            this.failedToLoadAd = true;
        }

        public void OnUnityAdsShowComplete (string placementId, UnityAdsShowCompletionState showCompletionState) {
            Novalog.Info("QuitButton", $"Ad finished showing -> {placementId} :: Completion State -> {showCompletionState}");
            GameOverEvents.Quit();
        }

        public void OnUnityAdsShowFailure (string placementId, UnityAdsShowError error, string message) {
            Novalog.Error("QuitButton", $"Ad failed to show Ad Unit -> {placementId}! -> {error.ToString()} :: {message}");
            GameOverEvents.Quit();
        }

        public void OnUnityAdsShowStart (string placementId) {}
        public void OnUnityAdsShowClick (string placementId) { }
    }
}
