using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class DenyAdButton : Button {

        public void OnMouseUp () {
            GameEvents.DenySecondChance();
        }
    }
}
