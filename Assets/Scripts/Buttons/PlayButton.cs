using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class PlayButton : Button {

        private void OnMouseUp () {
            StartMenuEvents.Play();
        }
    }
}
