using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class BackButton : Button {

        private void OnMouseUp () {
            ScoresEvents.Back();
        }
    }
}
