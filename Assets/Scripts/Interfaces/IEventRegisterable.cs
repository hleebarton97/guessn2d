using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    interface IEventRegisterable {
        void RegisterEventHandlers();
        void DeregisterEventHandlers();
    }
}
