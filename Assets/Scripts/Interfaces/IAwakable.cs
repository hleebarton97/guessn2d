using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    // Used for initializing data
    interface IAwakable {
        void SetupReferences();
        void Init();
    }
}
