using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    [CreateAssetMenu(menuName = "Novasloth/SO/Buttons/Simple")]
    public class ButtonSO : ScriptableObject {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public string Text { get { return this.text; } }

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private string text;
    }
}
