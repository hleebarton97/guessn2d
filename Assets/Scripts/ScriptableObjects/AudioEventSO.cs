using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
// Credit: https://github.com/richard-fine/scriptable-object-demo/blob/main/Assets/ScriptableObject/Audio/AudioEvent.cs
namespace Novasloth {
    public abstract class AudioEventSO : ScriptableObject {
        public abstract void Play (AudioSource audioSource);
    }
}
