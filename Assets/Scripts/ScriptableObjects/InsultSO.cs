using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    [CreateAssetMenu(menuName = "Novasloth/SO/Insults")]
    public class InsultSO : ScriptableObject {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private string[] insults;

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public string GetRandomInsult () {
            int index = Random.Range(0, insults.Length);
            return insults[index];
        }
    }
}
