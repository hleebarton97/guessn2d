using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
// Credit: https://github.com/richard-fine/scriptable-object-demo/blob/main/Assets/ScriptableObject/Audio/SimpleAudioEvent.cs
namespace Novasloth {

    [CreateAssetMenu(menuName="Novasloth/SO/Audio Event/Basic")]
    public class BasicAudioEventSO : AudioEventSO {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private AudioClip[] audioClips;
        [SerializeField] private RangedFloat volume;

        [MinMaxRange(0, 2)]
        [SerializeField] private RangedFloat pitch;

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public override void Play (AudioSource audioSource) {
            if (this.audioClips.Length > 0) {
                audioSource.clip = this.audioClips[Random.Range(0, this.audioClips.Length)];
                audioSource.volume = Random.Range(this.volume.minValue, this.volume.maxValue);
                audioSource.pitch = Random.Range(this.pitch.minValue, this.pitch.maxValue);

                audioSource.Play();
            }
            else {
                Novalog.Error("BasicAudioEventSO", "No audio clips to play!");
            }
        }
    }
}
