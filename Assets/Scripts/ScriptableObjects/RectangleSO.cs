using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    [CreateAssetMenu(menuName = "Novasloth/SO/Rectangle")]
    public class RectangleSO : ScriptableObject {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public int Id { get { return this.id; } }

        public Color CorrectColor { get { return this.correctColor; } }
        public Color WrongColor { get { return this.wrongColor; } }
        public Color DefaultColor {
            get { return this.defaultColor; }
            set { this.defaultColor = value; }
        }

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [Header("Properties")]
        [SerializeField] private int id;

        [Header("Colors")]
        [SerializeField] private Color correctColor;
        [SerializeField] private Color wrongColor;
        [SerializeField] private Color defaultColor;

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public Color GetColor (bool correct) {
            if (correct) {
                return this.correctColor;
            } else {
                return this.wrongColor;
            }
        }
    }
}
