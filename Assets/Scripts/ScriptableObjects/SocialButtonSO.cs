using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    [CreateAssetMenu(menuName = "Novasloth/SO/Buttons/Social")]
    public class SocialButtonSO : ButtonSO {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public string Link { get { return this.link; } }

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private string link;
    }
}
