using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    // Singleton handling data storage / loading into PlayerPrefs
    // Fully laxy instantiation
    public sealed class DataStore {

        ///////////////////////////////////////////////////////////////////
        // S I N G L E T O N
        ///////////////////////////////////////////////////////////////////

        private DataStore () { }

        public static DataStore Instance { get { return Nested.instance; } }

        private class Nested {
            static Nested () { }
            internal static readonly DataStore instance = new DataStore();
        }

        ///////////////////////////////////////////////////////////////////
        // G L O B A L   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        private const string KEY_HIGH_SCORE = "save_high_score";
        private const string KEY_HIGH_STREAK = "save_high_streak";

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public int HighScore { get; private set; }
        public int HighStreak { get; private set; }
        public int Score { get; private set; }
        public int Streak { get; private set; }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void TrySave (int highScore, int highStreak) {
            // Verify we are saving higher score and streak
            if (this.VerifyData(highScore, highStreak)) {
                // Save score and streak
                PlayerPrefs.SetInt(KEY_HIGH_SCORE, highScore);
                PlayerPrefs.SetInt(KEY_HIGH_STREAK, highStreak);

                // Update data
                this.HighScore = highScore;
                this.HighStreak = highStreak;

                Novalog.Info("DataStore", $"High score and high streak saved!");
            }
            else {
                Novalog.Warning("DataStore", $"High Score: {highScore} & High Streak: {highStreak} are not greater than currently stored values!");
            }
        }

        public void SaveLocal (int score, int streak) {
            this.Score = score;
            this.Streak = streak;
        }

        public void Load () {
            if (PlayerPrefs.HasKey(KEY_HIGH_SCORE)) {
                this.HighScore = PlayerPrefs.GetInt(KEY_HIGH_SCORE);
            } else {
                this.HighScore = 0;
            }

            if (PlayerPrefs.HasKey(KEY_HIGH_STREAK)) {
                this.HighStreak = PlayerPrefs.GetInt(KEY_HIGH_STREAK);
            } else {
                this.HighStreak = 0;
            }

            if (this.Score == 0 && this.Streak == 0) {
                Novalog.Warning("DataStore", "Game scores potentially not persisting?");
            }

            Novalog.Info("DataStore", $"Data loaded -> High Score: {this.HighScore}, High Streak: {this.HighStreak}.");
        }

        private bool VerifyData (int score, int streak) {
            this.Load();

            if (this.HighScore > score || this.HighStreak > streak) {
                return false;
            }

            return true;
        }
    }
}
