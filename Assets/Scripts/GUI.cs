using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class GUI : MonoBehaviour, IEventRegisterable {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private TextMeshProUGUI streakUI;
        [SerializeField] private TextMeshProUGUI scoreUI;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {
            GameEvents.StreakUpdatedEvent += this.OnStreakUpdated;
            GameEvents.ScoreUpdatedEvent += this.OnScoreUpdated;
        }

        public void DeregisterEventHandlers () {
            GameEvents.StreakUpdatedEvent -= this.OnStreakUpdated;
            GameEvents.ScoreUpdatedEvent -= this.OnScoreUpdated;
        }

        private void OnStreakUpdated (int streak) {
            this.streakUI.text = $"{streak}";
        }

        private void OnStrikesUpdated (int strikes) {

        }

        private void OnScoreUpdated (int score) {
            this.scoreUI.text = $"{score}";
        }
    }
}
