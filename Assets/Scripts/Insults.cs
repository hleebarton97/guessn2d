using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Insults : MonoBehaviour {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] InsultSO _Insults;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Start () {
            this.LoadInsult();
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void LoadInsult () {
            string insult = this._Insults.GetRandomInsult();
            GameOverEvents.InsultReady(insult);
        }
    }
}
