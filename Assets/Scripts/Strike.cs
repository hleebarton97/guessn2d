using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Strike : MonoBehaviour, IAwakable {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        private GameObject spriteDefault;
        private GameObject spritePressed;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.SetupReferences();
            this.Init();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () { }

        public void Init () {
            this.spriteDefault = this.transform.GetChild(0).gameObject;
            this.spritePressed = this.transform.GetChild(1).gameObject;
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void DoStrike () {
            this.spriteDefault.SetActive(false);
            this.spritePressed.SetActive(true);
        }

        public void FixStrike () {
            this.spriteDefault.SetActive(true);
            this.spritePressed.SetActive(false);
        }
    }
}
