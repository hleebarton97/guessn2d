using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Game : MonoBehaviour, IAwakable, IEventRegisterable {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public static bool IsPaused { get; private set; } = false;

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private Transform rectanglesParent;
        [SerializeField] private Transform strikesParent;
        private Dictionary<int, Strike> strikeLookup = new Dictionary<int, Strike>();

        [Header("Game")]
        [SerializeField] private int streak = 0;
        [SerializeField] private int bestStreak = 0;
        [SerializeField] private int score = 0;
        [SerializeField] private int strikes = 0;
        [SerializeField] private int pointsPer = 100;
        [SerializeField] private int bonus;

        [Header("Timers")]
        [SerializeField] private float resetTimerDelay;
        private Timer resetTimer;

        private bool hasSecondChance = true;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
            this.SetupReferences();
            this.Init();
        }

        private void Start () {
            this.NewRound();
        }

        private void Update () {
            this.resetTimer.Tick();
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () {}

        public void Init () {
            this.resetTimer = new Timer(this.resetTimerDelay, NewRound);
            this.bonus = 1;

            for (int i = 0; i < this.strikesParent.childCount; i++) {
                int strikeCount = i + 1;
                Strike strike = this.strikesParent.GetChild(i).GetComponent<Strike>();
                this.strikeLookup.Add(strikeCount, strike);
            }
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void SelectCorrectRectangle () {
            int index = Random.Range(0, this.rectanglesParent.childCount);
            GameEvents.RectangleSelected(index);
        }

        private void NewRound () {
            if (this.strikes < 3) {
                this.resetTimer.Stop();

                GameEvents.NewRound();

                this.SelectCorrectRectangle();
                IsPaused = false;
            } else {
                this.resetTimer.Stop();

                DataStore.Instance.SaveLocal(this.score, this.bestStreak);
                DataStore.Instance.TrySave(this.score, this.bestStreak);

                if (this.hasSecondChance) {
                    this.hasSecondChance = false;
                    GameEvents.PromptSecondChance();
                } else {
                    GameEvents.GameOver();
                }
            }
        }

        private void IncreaseStreak () {
            this.streak++;

            if (this.streak > this.bestStreak) {
                this.bestStreak = this.streak;
            }

            GameEvents.StreakUpdated(this.streak);
        }

        private void ResetStreak () {
            this.streak = 0;
            GameEvents.StreakUpdated(this.streak);
        }

        private void UpdateScore () {
            this.score += (this.pointsPer * this.bonus++);
            GameEvents.ScoreUpdated(this.score);
        }

        private void IncreaseStrikes () {
            this.strikes++;
            GameEvents.StrikesUpdated(this.strikes);
            this.bonus = 1;

            Strike strike;
            if (this.strikeLookup.TryGetValue(this.strikes, out strike)) {
                if (strike.isActiveAndEnabled) { strike.DoStrike(); }
            }
        }

        private void DecreaseStrikes () {
            if (this.strikes > 0) {
                Strike strike;
                if (this.strikeLookup.TryGetValue(this.strikes, out strike)) {
                    if (strike.isActiveAndEnabled) { strike.FixStrike(); }
                }

                this.strikes--;
            }
        }

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {
            GameEvents.CorrectButtonPressedEvent += this.OnCorrectButtonPressed;
            GameEvents.WrongButtonPressedEvent += this.OnWrongButtonPressed;

            GameEvents.DenySecondChanceEvent += this.OnDenySecondChance;
            GameEvents.AcceptSecondChanceEvent += this.OnAcceptSecondChance;
        }

        public void DeregisterEventHandlers () {
            GameEvents.CorrectButtonPressedEvent -= this.OnCorrectButtonPressed;
            GameEvents.WrongButtonPressedEvent -= this.OnWrongButtonPressed;

            GameEvents.DenySecondChanceEvent -= this.OnDenySecondChance;
            GameEvents.AcceptSecondChanceEvent -= this.OnAcceptSecondChance;
        }

        private void OnCorrectButtonPressed (int id) {
            IsPaused = true;
            this.resetTimer.Start();
            this.IncreaseStreak();
            this.DecreaseStrikes();
            this.UpdateScore();
        }

        private void OnWrongButtonPressed (int id) {
            IsPaused = true;
            this.resetTimer.Start();
            this.IncreaseStrikes();
            this.ResetStreak();
        }

        private void OnDenySecondChance () {
            GameEvents.GameOver();
        }

        private void OnAcceptSecondChance () {
            this.ResetStreak();
            this.DecreaseStrikes();
            this.NewRound();
        }
    }
}
