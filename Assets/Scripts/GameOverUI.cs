using UnityEngine;
using TMPro;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class GameOverUI : MonoBehaviour, IAwakable, IEventRegisterable {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private TextMeshProUGUI insultUI;
        [SerializeField] private TextMeshProUGUI scoreUI;
        [SerializeField] private TextMeshProUGUI highScoreUI;
        [SerializeField] private TextMeshProUGUI streakUI;
        [SerializeField] private TextMeshProUGUI highStreakUI;

        [SerializeField] private Color valuesColor;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
            this.SetupReferences();
            this.Init();
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () {}

        public void Init () {
            DataStore.Instance.Load();

            this.streakUI.text = this.GetStreakText(DataStore.Instance.Streak);
            this.scoreUI.text = this.GetScoreText(DataStore.Instance.Score);
            this.highStreakUI.text = this.GetHighStreakText(DataStore.Instance.HighStreak);
            this.highScoreUI.text = this.GetHighScoreText(DataStore.Instance.HighScore);
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private string GetStreakText (int streak) => $"Streak: {this.WrapColor(streak)}";
        private string GetScoreText (int score) => $"Score: {this.WrapColor(score)}";
        private string GetHighStreakText (int highStreak) => $"High Streak: {this.WrapColor(highStreak)}";
        private string GetHighScoreText (int highScore) => $"High Score: {this.WrapColor(highScore)}";

        private string WrapColor (int value) => $"<color=#{ColorUtility.ToHtmlStringRGBA(this.valuesColor)}>{value}</color>";

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {
            GameOverEvents.InsultReadyEvent += this.OnInsultReady;
        }

        public void DeregisterEventHandlers () {
            GameOverEvents.InsultReadyEvent -= this.OnInsultReady;
        }

        private void OnInsultReady (string insult) {
            this.insultUI.text = insult;
        }
    }
}
