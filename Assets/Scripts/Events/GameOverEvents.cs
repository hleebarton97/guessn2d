using System;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public static class GameOverEvents {

        ///////////////////////////////////////////////////////////////////
        // E V E N T S
        ///////////////////////////////////////////////////////////////////

        public static event Action<string> InsultReadyEvent;
        public static event Action PlayAgainEvent;
        public static event Action QuitEvent;

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        // Play Again button has been pressed.
        public static void PlayAgain () {
            Novalog.Info("GameOverEvents", "Play again pressed!");
            PlayAgainEvent?.Invoke();
        }

        // Quit button has been pressed.
        public static void Quit () {
            Novalog.Info("GameOverEvents", "Quit pressed!");
            QuitEvent?.Invoke();
        }

        // Insult string has been selected to be displayed.
        public static void InsultReady (string insult) {
            Novalog.Info("GameOverEvents", $"Insult ready -> {insult}");
            InsultReadyEvent?.Invoke(insult);
        }
    }
}
