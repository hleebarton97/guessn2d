using System;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public static class StartMenuEvents {

        ///////////////////////////////////////////////////////////////////
        // E V E N T S
        ///////////////////////////////////////////////////////////////////

        public static event Action PlayEvent;
        public static event Action ExitEvent;
        public static event Action ShowScoresEvent;

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public static void Play () {
            Novalog.Info("StartMenuEvents", $"Play pressed!");
            PlayEvent?.Invoke();
        }

        // Display the player's high score and streak.
        public static void ShowScores () {
            Novalog.Info("StartMenuEvents", "Scores pressed!");
            ShowScoresEvent?.Invoke();
        }

        public static void Exit () {
            Novalog.Info("StartMenuEvents", $"Exit pressed!");
            ExitEvent?.Invoke();
        }
    }
}
