using System;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public static class GameEvents {

        ///////////////////////////////////////////////////////////////////
        // E V E N T S
        ///////////////////////////////////////////////////////////////////

        public static event Action NewRoundEvent;
        public static event Action GameOverEvent;

        public static event Action PromptSecondChanceEvent;
        public static event Action AcceptSecondChanceEvent;
        public static event Action DenySecondChanceEvent;

        public static event Action<int> CorrectButtonPressedEvent;
        public static event Action<int> WrongButtonPressedEvent;
        public static event Action<int> RectangleSelectedEvent;
        public static event Action<int> StreakUpdatedEvent;
        public static event Action<int> ScoreUpdatedEvent;
        public static event Action<int> StrikesUpdatedEvent;

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        // Correct rectangle has been pressed
        public static void CorrectButtonPressed (int id) {
            Novalog.Info("GameEvents", $"CORRECT rectangle pressed -> {id}");
            CorrectButtonPressedEvent?.Invoke(id);
        }

        // Incorrect rectangle has been pressed
        public static void WrongButtonPressed (int id) {
            Novalog.Info("GameEvents", $"WRONG rectangle pressed -> {id}");
            WrongButtonPressedEvent?.Invoke(id);
        }

        // Correct rectangle has been randomly selected
        public static void RectangleSelected (int id) {
            Novalog.Info("GameEvents", $"Rectangle randomly selected -> {id}");
            RectangleSelectedEvent?.Invoke(id);
        }

        // Start new round
        public static void NewRound () {
            Novalog.Info("GameEvents", $"Starting new round...");
            NewRoundEvent?.Invoke();
        }

        // End of the game
        public static void GameOver () {
            Novalog.Info("GameEvents", $"Game is over...");
            GameOverEvent?.Invoke();
        }

        // Check if player wants a second chance
        public static void PromptSecondChance () {
            Novalog.Info("GameEvents", "Prompting player for second chance...");
            PromptSecondChanceEvent?.Invoke();
        }

        // Player has accepted and watched ad for second chance
        public static void AcceptSecondChance () {
            Novalog.Info("GameEvents", "Player has watched ad for second chance!");
            AcceptSecondChanceEvent?.Invoke();
        }

        // Player has denied second chance
        public static void DenySecondChance () {
            Novalog.Info("GameEvents", "Player has denied second chance!");
            DenySecondChanceEvent?.Invoke();
        }

        // Streak has been updated in value
        public static void StreakUpdated (int streak) {
            Novalog.Info("GameEvents", $"Streak updated -> {streak}");
            StreakUpdatedEvent?.Invoke(streak);
        }

        // Strikes has been updated in value
        public static void StrikesUpdated (int strikes) {
            Novalog.Info("GameEvents", $"Strikes updated -> {strikes}");
            StrikesUpdatedEvent?.Invoke(strikes);
        }

        // Score has been updated in value
        public static void ScoreUpdated (int score) {
            Novalog.Info("GameEvents", $"Score updated -> {score}");
            ScoreUpdatedEvent?.Invoke(score);
        }
    }
}
