using UnityEngine;
using UnityEngine.Advertisements;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class AdsInit : MonoBehaviour, IUnityAdsInitializationListener, IAwakable {

        private static GameObject Instance;

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [Header("Game IDs")]
        [SerializeField] private string androidGameId;
        [SerializeField] private string iOSGameId;
        private string gameId;

        [Header("Debug")]
        [SerializeField] private bool test = false;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.SetupReferences();
            this.Init();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () {}

        public void Init () {
            if (!Util.IsNull(Instance)) {
                Destroy(Instance);
            }

            Instance = this.gameObject;
            DontDestroyOnLoad(this);

            this.InitAds();
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void InitAds () {
            this.gameId = (Application.platform == RuntimePlatform.IPhonePlayer || SystemInfo.deviceModel.Contains("iPad"))
                ? this.iOSGameId
                : this.androidGameId;

            Advertisement.Initialize(this.gameId, this.test, this);
        }

        ///////////////////////////////////////////////////////////////////
        // L I S T E N E R S
        ///////////////////////////////////////////////////////////////////

        public void OnInitializationComplete () {
            Novalog.Info("AdsInit", $"Unity Ads initialization complete -> {this.gameId}");
        }

        public void OnInitializationFailed (UnityAdsInitializationError error, string message) {
            Novalog.Error("AdsInit", $"Unity Ads initialization failed -> {error.ToString()} :: {message}");
        }


    }
}
