using System;

// Novasloth Games LLC
// Lee Barton
// Credit: https://github.com/richard-fine/scriptable-object-demo/blob/main/Assets/ScriptableObject/Audio/MinMaxRangeAttribute.cs
namespace Novasloth {
    public class MinMaxRangeAttribute : Attribute {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public float Min { get; private set; }
        public float Max { get; private set; }

        ///////////////////////////////////////////////////////////////////
        // C O N S T R U C T O R
        ///////////////////////////////////////////////////////////////////

        public MinMaxRangeAttribute (float min, float max) {
            this.Min = min;
            this.Max = max;
        }
    }
}
