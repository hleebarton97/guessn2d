using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Rectangle : MonoBehaviour, IAwakable, IEventRegisterable {

        ///////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        public bool IsPressed { get; set; } = false;
        public bool IsCorrect { get; set; } = false;

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private RectangleSO _Rectangle;

        private GameObject spriteDefault;
        private GameObject spritePressed;
        private SpriteRenderer spriteRendererDefault;
        private SpriteRenderer spriteRendererPressed;

        [Header("SFX")]
        [SerializeField] private AudioEventSO _AudioEventCorrect;
        [SerializeField] private AudioEventSO _AudioEventWrong;
        [SerializeField] private AudioSource audioSource;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
            this.SetupReferences();
            this.Init();
        }

        private void OnMouseDown () {
            if (!Game.IsPaused) {
                this.CheckPressed();
            }
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () {
            if (Util.IsNull(this.audioSource)) {
                this.audioSource = Util.SetupAudioSource(this.gameObject);
            }
        }

        public void Init () {
            this.InitSprites();
        }

        // Initialize sprite gameobjects
        private void InitSprites () {
            if (this.transform.childCount != 2) {
                Novalog.Error(this.GetType().Name, "Missing or too many sprite child objects!");
                return;
            }

            this.spriteDefault = this.transform.GetChild(0).gameObject;
            this.spritePressed = this.transform.GetChild(1).gameObject;
            this.spriteRendererDefault = this.spriteDefault.GetComponent<SpriteRenderer>();
            this.spriteRendererPressed = this.spritePressed.GetComponent<SpriteRenderer>();

            this._Rectangle.DefaultColor = this.spriteRendererDefault.color;
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void CheckPressed () {
            if (!this.IsPressed) {
                Novalog.Info(this.GetType().Name, $"Rectangle {this._Rectangle.Id} pressed!");
                this.PressButton();
            }
        }

        private void PressButton () {
            this.IsPressed = true;
            this.spriteDefault.SetActive(false);
            this.spritePressed.SetActive(true);

            int id = this._Rectangle.Id;
            bool correct = this.IsCorrect;
            if (correct) {
                this._AudioEventCorrect.Play(this.audioSource);
                GameEvents.CorrectButtonPressed(id);
            } else {
                this._AudioEventWrong.Play(this.audioSource);
                GameEvents.WrongButtonPressed(id);
            }
        }

        private void FixButton () {
            this.IsPressed = false;
            this.IsCorrect = false;

            this.spriteRendererDefault.color = this._Rectangle.DefaultColor;
            this.spriteRendererPressed.color = this._Rectangle.DefaultColor;

            this.spriteDefault.SetActive(true);
            this.spritePressed.SetActive(false);
        }

        private void SetPressedColor () {
            Color color = this._Rectangle.GetColor(this.IsCorrect);
            Novalog.Info("Rectangle", $"Setting Pressed Color {color}");
            this.spriteRendererPressed.color = color;
        }

        private void SetUnpressedColor () {
            this.spriteRendererDefault.color = this._Rectangle.GetColor(this.IsCorrect);
        }

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {
            GameEvents.CorrectButtonPressedEvent += this.OnButtonPressed;
            GameEvents.WrongButtonPressedEvent += this.OnButtonPressed;
            GameEvents.RectangleSelectedEvent += this.OnRectangleSelected;
            GameEvents.NewRoundEvent += this.OnNewRound;
        }

        public void DeregisterEventHandlers () {
            GameEvents.CorrectButtonPressedEvent -= this.OnButtonPressed;
            GameEvents.WrongButtonPressedEvent -= this.OnButtonPressed;
            GameEvents.RectangleSelectedEvent -= this.OnRectangleSelected;
            GameEvents.NewRoundEvent -= this.OnNewRound;
        }

        private void OnButtonPressed (int id) {
            if (this._Rectangle.Id == id) {
                this.SetPressedColor();
            } else {
                this.SetUnpressedColor();
            }
        }

        private void OnRectangleSelected (int id) {
            if (this._Rectangle.Id == id) {
                this.IsCorrect = true;
            } else {
                this.IsCorrect = false;
            }
        }

        private void OnNewRound () {
            this.FixButton();
        }
    }
}
