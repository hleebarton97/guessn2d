using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    // Unity Debug.Log wrapper class
    public static class Novalog {

        public static void Info (string className, string msg) {
            Debug.Log($"<color=cyan>INFO :: {className}</color> :: {msg}");
        }

        public static void Warning (string className, string msg) {
            Debug.LogWarning($"<color=orange>WARNING :: {className}</color> :: {msg}");
        }

        public static void Error (string className, string msg) {
            Debug.LogError($"<color=red>ERROR :: {className}</color> :: {msg}");
        }
    }
}
