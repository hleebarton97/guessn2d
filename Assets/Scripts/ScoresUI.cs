using UnityEngine;
using TMPro;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class ScoresUI : MonoBehaviour, IAwakable, IEventRegisterable {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private TextMeshProUGUI highStreakUI;
        [SerializeField] private TextMeshProUGUI highScoreUI;

        [SerializeField] private Color valuesColor;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
            this.SetupReferences();
            this.Init();
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // I N I T   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        public void SetupReferences () { }

        public void Init () {
            DataStore.Instance.Load();

            this.highStreakUI.text = this.ValueToString(DataStore.Instance.HighStreak);
            this.highScoreUI.text = this.ValueToString(DataStore.Instance.HighScore);
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private string ValueToString (int value) => $"{this.WrapColor(value)}";
        private string WrapColor (int value) => $"<color=#{ColorUtility.ToHtmlStringRGBA(this.valuesColor)}>{value}</color>";

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {

        }

        public void DeregisterEventHandlers () {

        }
    }
}
