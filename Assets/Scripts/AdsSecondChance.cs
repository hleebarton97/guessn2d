using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class AdsSecondChance : MonoBehaviour, IEventRegisterable {

        ///////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        [SerializeField] private GameObject secondChanceUI;

        ///////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
        }

        private void OnDestroy () {
            this.DeregisterEventHandlers();
        }

        ///////////////////////////////////////////////////////////////////
        // E V E N T   H A N D L E R S
        ///////////////////////////////////////////////////////////////////

        public void RegisterEventHandlers () {
            GameEvents.PromptSecondChanceEvent += this.OnPromptSecondChance;
            GameEvents.AcceptSecondChanceEvent += this.OnAcceptSecondChance;
        }

        public void DeregisterEventHandlers () {
            GameEvents.PromptSecondChanceEvent -= this.OnPromptSecondChance;
            GameEvents.AcceptSecondChanceEvent -= this.OnAcceptSecondChance;
        }

        private void OnPromptSecondChance () {
            this.secondChanceUI.SetActive(true);
        }

        private void OnAcceptSecondChance () {
            this.secondChanceUI.SetActive(false);
        }
    }
}
