using UnityEngine;
using UnityEngine.SceneManagement;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public class Screenshot : MonoBehaviour {

        private void Update () {
            if (Input.GetButtonDown("Fire1")) {
                Novalog.Info("Screenshot", "Screenshot taken!");
                ScreenCapture.CaptureScreenshot("ss_" + SceneManager.GetActiveScene().name + ".png");
            }
        }
    }
}
