using UnityEngine;
using UnityEngine.Audio;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public static class Util {

        // Check if Unity object is null or behaving like it should be null.
        public static bool IsNull (object obj) {
            return obj == null || obj.Equals(null);
        }

        // Create an audio source for audio events.
        public static AudioSource SetupAudioSource (GameObject gameObject) {
            AudioSource audioSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
            AudioMixer mainMixer = Resources.Load<AudioMixer>("Mixers/Main");
            AudioMixerGroup masterGroup = mainMixer.FindMatchingGroups("Master")[0];

            audioSource.outputAudioMixerGroup = masterGroup;

            return audioSource;
        }
    }
}
