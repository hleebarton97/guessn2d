:: Check if path to unity.exe exists
if "%UNITY_PATH%"=="" (
  SET UNITY_PATH=C:\Program Files\Unity\Hub\Editor\2021.3.0f1\Editor\Unity.exe
)

:: Check for specified APK directory
if "%ANDROID_APK_OUTPUT_DIR%"=="" (
  echo ANDROID_APK_OUTPUT_DIR is undefined
  echo Setting ANDROID_APK_OUTPUT_DIR to 'build\android'
  set ANDROID_APK_OUTPUT_DIR=build\android
  exit /B 1
)

:: Check for APK filename
if "%ANDROID_APK_FILENAME%"=="" (
  echo ANDROID_APK_FILENAME is undefined
  echo Setting ANDROID_APK_FILENAME to 'novasloth-game.apk'
  set ANDROID_APK_FILENAME=novasloth-game.apk
  exit /B 1
)

:: Set APK full path once we know they exist
SET APK=%ANDROID_APK_OUTPUT_DIR%\%ANDROID_APK_FILENAME%

:: Create APK file directory, delete if APK file already exists
echo Creating build directory...
mkdir %ANDROID_APK_OUTPUT_DIR%
if exist %APK% (
  echo APK %APK% already exists! Cleaning up directory...
  del /F %APK%
)
echo Build directory created!

:: Start the Unity build process
echo Starting build process...
"%UNITY_PATH%" -quit -batchmode -logFile build.log -executeMethod Novasloth.Build.StartAndroidBuild %APK%

:: Check for successful build
if not exist %APK% (
  echo Error in build process, please check build.log file for details!
  exit /B 1
) else (
  echo Build successful! Artifact created: %APK%
)